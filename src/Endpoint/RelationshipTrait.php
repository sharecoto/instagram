<?php
namespace Sharecoto\Instagram\Endpoint;

trait RelationshipTrait
{
    public function postRelation($targetUser, $action = 'follow')
    {
        $endpoint = $this->apiUrl . 'users/%s/relationship';
        $body = array(
            'access_token' => $this->token,
            'action' => $action
        );
        $sig = $this->getSigParam(sprintf('/users/%s/relationship', $targetUser), $body);
        $body['sig'] = $sig;

        error_log(var_export($body, true));
        $response = $this->httpClient->post(
            sprintf($endpoint, urlencode($targetUser)),
            [
                'headers' => ['X-Insta-Forwarded-For' => $this->getSignedHeader()],
                'body' => $body
            ]
        );
        return $response->json()['data'];
    }
}

