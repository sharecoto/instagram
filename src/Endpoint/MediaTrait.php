<?php
namespace Sharecoto\Instagram\Endpoint;

trait MediaTrait
{
    /**
     * @see http://instagram.com/developer/endpoints/media/
     * @param  string $mediaId
     * @return array
     */
    public function media($mediaId)
    {
        $client = $this->httpClient;
        $query = $this->requestParams();
        $endpoint = $this->apiUrl.'media/%s';
        $response = $client->get(
            sprintf($endpoint, $mediaId),
            [
                'query' => $query
            ]
        );

        return $response->json()['data'];
    }
}
