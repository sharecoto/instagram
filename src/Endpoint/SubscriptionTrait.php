<?php
namespace Sharecoto\Instagram\Endpoint;

trait SubscriptionTrait
{
    /**
     * 現在の購読リスト
     *
     * @see http://instagram.com/developer/realtime/
     */
    public function getSubscription()
    {
        $endpoint = $this->apiUrl . 'subscriptions';
        $query = $this->requestParams();
        $response = $this->httpClient->get(
            $endpoint,
            [
                'query' => $query
            ]
        );

        return $response->json()['data'];
    }

    /**
     * 引数で指定されたIDを購読しているかどうかを判定する
     *
     * @param string $id numeric
     * @return bool
     */
    public function isSubscribeIn($id)
    {
        $subscriptions = $this->getSubscription();

        $subscribeIn = false;
        foreach ($subscriptions as $s) {
            if (isset($s['id']) && ($s['id'] == $id)) {
                $subscribeIn = true;
            }
        }

        // 購読中フラグがfalseだったらDBからレコードを削除しておく
        if (!$subscribeIn) {
            \Model\Subscription::where('subscription_id', $id)->first()->delete();
        }

        return $subscribeIn;
    }

    /**
     * 引数のパラメータはInstagramのAPIドキュメントを参照すること
     *
     * @param array $params
     */
    public function createSubscription(array $params)
    {
        $endpoint = $this->apiUrl.'subscriptions';
        $params = array_merge(
            array(
                'verify_token' => $this->getVerifyToken(),
                'callback_url' => $this->getSubscriptionCallbackUrl(),
                'aspect' => 'media',
                'client_id' => isset($this->config['client_id']) ? $this->config['client_id'] : null,
                'client_secret' => isset($this->config['client_secret']) ? $this->config['client_secret'] : null
            ),
            $params
        );

        $response = $this->httpClient->post(
            $endpoint,
            [
                'body' => $params,
            ]
        );
        return $response->json()['data'];
    }

    /**
     * 現状verify_tokenを照合する機能はないけど、
     * 一応メソッドを分解しておきます。
     *
     * @return string
     */
    public function getVerifyToken()
    {
        return md5(rand());
    }

    /**
     * Subscription CallbackUrl
     * Slimに依存
     */
    public function getSubscriptionCallbackUrl()
    {
        $slim = \Slim\Slim::getInstance();
        $url = $slim->request->getUrl() . $slim->urlFor('instagram_subscription');
        return $url;
    }
}
