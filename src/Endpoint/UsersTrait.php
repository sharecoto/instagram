<?php

namespace Sharecoto\Instagram\Endpoint;

trait UsersTrait
{
    /**
     * Get basic information about a user.
     * access_tokenがあるときに限り、selfで自分の情報を取得できる
     *
     * @param  numeric $user instagram user id
     * @return array
     */
    public function users($id = 'self')
    {
        $endpoint = $this->apiUrl.'users/%s';
        $query = $this->requestParams();
        $response = $this->httpClient->get(
            sprintf($endpoint, urlencode($id)),
            [
                'query' => $query
            ]
        );

        return $response->json()['data'];
    }

    /**
     * ユーザーIDから、メディアを取得
     *
     * @param  numeric $user instagram user id | 'self'
     * @return array
     */
    public function usersRecent($id, $params = [])
    {
        $endpoint = $this->apiUrl . 'users/%s/media/recent';

        $query = $this->requestParams($params);
        $response = $this->httpClient->get(
            sprintf($endpoint, urlencode($id)),
            [
                'query' => $query
            ]
        );
        return $response->json()['data'];
    }

    public function findByName($username)
    {
        $endpoint = $this->apiUrl.'users/search';
        $query = $this->requestParams(['q' => $username]);
        $response = $this->httpClient->get(
            $endpoint,
            [
                'query' => $query
            ]
        );

        $data = $response->json()['data'];
        foreach ($data as $u) {
            if (strtolower($u['username']) === $username) {
                return $u;
            }
        }
        return null;
    }
}
