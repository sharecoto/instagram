<?php
/**
 * Trait. タグ関連エンドポイントをコールするメソッド
 *
 * @author fujii@sharecoto.co.jp
 * @see http://instagram.com/developer/endpoints/tags/
 */

namespace Sharecoto\Instagram\Endpoint;

trait TagsTrait
{
    /**
     * Get information about a tag object.
     *
     * @param string $tag
     * @return array
     */
    public function tags($tag)
    {
        $endpoint = $this->apiUrl . 'tags/%s';

        $query = $this->requestParams();
        $sig = $this->getSigParam(
            sprintf('/tags/%s', urlencode($tag)),
            $query);
        $query['sig'] = $sig;

        $response = $this->httpClient->get(
            sprintf($endpoint, urlencode($tag)),
            [
                'query' => $query
            ]
        );
        return $response->json()['data'];
    }

    /**
     * Get a list of recently tagged media.
     *
     * @param string $tag
     * @param array  $params
     * @return array
     */
    public function tagsRecent($tag, $params = [])
    {
        $endpoint = $this->apiUrl . 'tags/%s/media/recent';

        $query = $this->requestParams($params);
        $sig = $this->getSigParam(
            sprintf('/tags/%s/media/recent', urlencode($tag)),
            $query);
        $query['sig'] = $sig;

        $response = $this->httpClient->get(
            sprintf($endpoint, urlencode($tag)),
            [
                'query' => $query
            ]
        );
        return $response->json()['data'];
    }
}
