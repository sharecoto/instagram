<?php
namespace Sharecoto\Instagram;

class Instagram
{
    // traits
    use Endpoint\MediaTrait,
        Endpoint\UsersTrait,
        Endpoint\TagsTrait,
        Endpoint\SubscriptionTrait,
        Endpoint\RelationshipTrait;

    /**
     * EndpointのbaseUrl
     *
     * @var string
     */
    protected $apiUrl = 'https://api.instagram.com/v1/';

    /**
     * OAuthのurl
     *
     * @var string
     */
    protected $oauthUrl = 'https://api.instagram.com/oauth/authorize/';

    /**
     * OAuth TokenのURL
     *
     * @var string
     */
    protected $tokenUrl = 'https://api.instagram.com/oauth/access_token';

    /**
     * app config
     *
     * @var array
     */
    protected $config = array(
        'client_id'     => null,
        'client_secret' => null,
        'redirect_uri' => null,
        'scope' => null,
    );

    /**
     * access token
     *
     * @var string
     */
    protected $token;

    /**
     * ログイン中のユーザー情報
     *
     * @var array
     */
    protected $currentUser;

    /**
     * http client
     *
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    public function __construct(array $config)
    {
        $this->config = array_intersect_key(
            $config,
            $this->config
        );

        $this->setHttpClient();
    }

    public function setHttpClient($client = null)
    {
        if (!$client) {
            $client = new \GuzzleHttp\Client();
        }

        $this->httpClient = $client;
    }

    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * コンフィグに値を投入
     *
     * @param string $key
     * @param mixed  $value
     */
    public function setConfig($key, $value)
    {
        $this->config[$key] = $value;

        return $this;
    }

    /**
     * コンフィグを返却
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * OAuthログイン用のURLを返す
     */
    public function getLoginUrl($responseType = 'code', array $callbackParams = null)
    {
        $redirectUri = isset($this->config['redirect_uri']) ? $this->config['redirect_uri'] : '';
        if ($callbackParams) {
            $redirectUri .= '?' . http_build_query($callbackParams);
        }

        $param = $this->requestParams([
            'response_type' => $responseType,
            'redirect_uri'  => $redirectUri
        ], false);
        $url = $this->oauthUrl . '?' . http_build_query($param);
        return $url;
    }

    /**
     * アクセストークンをセット
     *
     * @param string $token
     */
    public function setAccessToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * $this->tokeをそのまま返す
     *
     * @param string $code
     * @return string|false
     */
    public function getAccessToken()
    {
        if ($this->token) {
            return $this->token;
        }

        return false;
    }

    /**
     * 認証済ユーザーの情報をセット
     */
    public function setCurrentUser(array $user)
    {
        $this->currentUser = $user;
        return $this;
    }

    /**
     * 認証済ユーザーを返す
     *
     * @return array
     */
    public function getCurrentUser()
    {
        return $this->currentUser;
    }


    /**
     * InstagramのAPIからアクセストークンを取得する
     *
     * @return array
     */
    public function getOAuthToken($code, array $redirectParam = null)
    {
        $redirectUri = $this->config['redirect_uri'];
        if ($redirectParam) {
            $redirectUri .= '?' . http_build_query($redirectParam);
        }

        $param = $this->requestParams(
            [
                'grant_type' => 'authorization_code',
                'code' => $code,
                'redirect_uri' => $redirectUri
            ]
        );
        unset($param['scope']);

        $response = $this->httpClient->post(
            $this->tokenUrl,
            [
                'body' => $param
            ]
        );
        $payload = $response->json();

        if (isset($payload['access_token'])) {
            $this->setAccessToken($payload['access_token']);
        }
        if (isset($payload['user'])) {
            $this->setCurrentUser($payload['user']);
        }

        return $payload;
    }

    /**
     * HTTPリクエスト用のパラメーターを組み立てて配列を返す
     */
    public function requestParams($params = [], $tokenParam = true)
    {
        $defaultParam = $this->config;
        if ($this->token && $tokenParam) {
            $defaultParam = ['access_token' => $this->token];
        }

        return array_merge(
            $defaultParam,
            $params
        );
    }

    public function getSignedHeader($ip = null)
    {
        if (!$ip) {
            $ip = $_SERVER['SERVER_ADDR'];
        }
        $signature = hash_hmac('sha256', $ip, $this->config['client_secret'], false);
        return implode('|', array($ip, $signature));
    }

    /**
     * @param string $endpoint
     * @param array $params
     */
    public function getSigParam($endpoint, $params)
    {
        $sig = $endpoint;
        ksort($params);
        foreach ($params as $key => $val) {
            $sig .= sprintf("|%s=%s", $key, $val);
        }
        $signature = hash_hmac('sha256', $sig, $this->config['client_secret'], false);
        return $signature;
    }

}
