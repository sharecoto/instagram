<?php
use Sharecoto\Instagram\Instagram;
class UsersTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $client = _getClient();
        $this->instagram = new Instagram($client);
    }

    public function testFetchUser()
    {
        $id = '1580938135';
        $user = $this->instagram->users($id);
    }

    public function testFetchUserRecent()
    {
        $id = '1580938135';
        $media = $this->instagram->usersRecent($id);
    }
}
