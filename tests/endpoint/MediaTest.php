<?php
class MediaTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->instagram = new \Sharecoto\Instagram\Instagram(
            _getClient()
        );
    }

    public function testFetchMedia()
    {
        $media = $this->instagram->media('1059344300859893905_477671449');
        $this->assertTrue(is_array($media));
    }
}
